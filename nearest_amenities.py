import pandas as pd
import sys, PIL.Image, pprint
from PIL.ExifTags import TAGS, GPSTAGS


def get_gps_exif(photo_name):
    img = PIL.Image.open(photo_name)
    exif = {
        TAGS[k]: v
        for k, v in img._getexif().items()
        if k in PIL.ExifTags.TAGS
    }

    gps_exif = {}
    for key in exif['GPSInfo'].keys():
        decode = GPSTAGS.get(key,key)
        gps_exif[decode] = exif['GPSInfo'][key]
    
    return gps_exif


# convert DMS to decimal
def get_lat_lon(gps_exif):
    dms_lat = gps_exif['GPSLatitude']
    dms_lon = gps_exif['GPSLongitude']
    decimal_lat = dms_lat[0][0] + dms_lat[1][0]/(60*dms_lat[1][1]) + dms_lat[2][0]/(3600*dms_lat[2][1])
    decimal_lon = dms_lon[0][0] + dms_lon[1][0]/(60*dms_lon[1][1]) + dms_lon[2][0]/(3600*dms_lon[2][1])
    
    if (gps_exif['GPSLongitudeRef'] == 'W'):
        decimal_lon = -1 * decimal_lon
    
    if (gps_exif['GPSLatitudeRef'] == 'S'):
        decimal_lat = -1 * decimal_lat
    
    return {'lat': decimal_lat, 'lon': decimal_lon}


def get_amenities():
    file_name = "amenities-vancouver.json.gz"
    amenities = pd.read_json(file_name, lines=True, compression='gzip')
    amenities = amenities[~amenities['name'].isnull()]
    return amenities


# output amenities within a square with length 2*N_meters surrounding the photo's coordinates
def get_nearby_amenities(photo_name, N_meters, amenities):
    gps_exif = get_gps_exif(photo_name)
    lat_lon = get_lat_lon(gps_exif)

    # "Around Vancouver, one degree of latitude or longitude is about 10^5 meters. That will be a close enough conversion as we're estimating error…"
    # per hint given in Exercise 3 for calc_distance.py
    meter_per_degree = 100000
    
    # filter out amenities outside the square with length 2*N_meters
    distance_from_center_in_degrees = N_meters/meter_per_degree
    amenities = amenities[amenities['lat'] <= lat_lon['lat']+distance_from_center_in_degrees]
    amenities = amenities[amenities['lat'] >= lat_lon['lat']-distance_from_center_in_degrees]
    amenities = amenities[amenities['lon'] <= lat_lon['lon']+distance_from_center_in_degrees]
    amenities = amenities[amenities['lon'] >= lat_lon['lon']-distance_from_center_in_degrees]
    
    return amenities


def get_category_amenities(amenities, category):
    family_friendly = ['arts_centre', 'bbq', 'bistro', 'cafe', 'clock', 'community_centre', 'family_centre', 'ice_cream', 'juice_bar', 'leisure', 'library', 'marketplace', 'park', 'photo_booth', 'playground', 'restaurant', 'seaplane_terminal', 'theatre', 'science']
    foodie = ['bbq', 'bistro', 'cafe', 'restaurant']
    dessert_crawl = ['cafe', 'ice_cream']
    pub_crawl = ['bar', 'biergarten', 'pub']
    tourist = ['clock', 'cafe', 'seaplane_terminal', 'park', 'marketplace', 'leisure', 'restaurant', 'biergarten', 'theatre', 'science']

    if category == "family_friendly":
        return amenities[amenities['amenity'].isin(family_friendly)]
    elif category == "foodie":
        return amenities[amenities['amenity'].isin(foodie)]
    elif category == "dessert_crawl":
        return amenities[amenities['amenity'].isin(dessert_crawl)]
    elif category == "tourist":
        return amenities[amenities['amenity'].isin(tourist)]
    elif category == "pub_crawl":
        return amenities[amenities['amenity'].isin(pub_crawl)]
    else:
        return amenities  


def main():
    photo_name = 'sample.jpg'
    N_meters = 1000

    amenities = get_amenities()
    amenities = get_nearby_amenities(photo_name, N_meters, amenities)

    family_friendly_amenities = get_category_amenities(amenities,"family_friendly")
    foodie_amenities = get_category_amenities(amenities,"foodie")
    dessert_crawl_amenities = get_category_amenities(amenities,"dessert_crawl")
    tourist_amenities = get_category_amenities(amenities,"tourist")
    pub_crawl_amenities = get_category_amenities(amenities,"pub_crawl")

if __name__ == "__main__":
    main()