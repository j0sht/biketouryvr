import sys
import PIL.Image
from PIL.ExifTags import TAGS, GPSTAGS
import numpy as np
import pandas as pd
import math
from datetime import datetime
import googlemaps
import requests
from urllib.parse import urlencode
import re

USER_PHOTO = 'sample.jpg'
FRIEND_PHOTO = 'stanley.jpeg'
N_METERS = 10000
API_KEY = "AIzaSyDqsofVuBdWx_kW2Mt-Du-y-ZIIbfnXYVs"

GMAPS_DIR = googlemaps.Client(key=API_KEY)

def distance(lat1, lon1, lat2, lon2):
    p = math.pi/180
    clat = lat1
    clon = lon1

    slat = lat2
    slon = lon2
    
    x = 0.5 - np.cos((slat-clat)*p)/2 + np.cos(clat*p) * np.cos(slat*p) * (1- np.cos((slon-clon)*p))/2
    d = 12742 * np.arcsin(np.sqrt(x))
    
    # km
    return d

def get_gps_exif(photo_name):
    img = PIL.Image.open(photo_name)
    exif = {
        TAGS[k]: v
        for k, v in img._getexif().items()
        if k in PIL.ExifTags.TAGS
    }

    gps_exif = {}
    for key in exif['GPSInfo'].keys():
        decode = GPSTAGS.get(key,key)
        gps_exif[decode] = exif['GPSInfo'][key]
    
    return gps_exif

def get_lat_lon(gps_exif):
    dms_lat = gps_exif['GPSLatitude']
    dms_lon = gps_exif['GPSLongitude']
    decimal_lat = dms_lat[0] + dms_lat[1]/(60) + dms_lat[2]/(3600)
    decimal_lon = dms_lon[0] + dms_lon[1]/(60) + dms_lon[2]/(3600)
    
    if (gps_exif['GPSLongitudeRef'] == 'W'):
        decimal_lon = -1 * decimal_lon
    
    if (gps_exif['GPSLatitudeRef'] == 'S'):
        decimal_lat = -1 * decimal_lat
    
    #Convert it to float
    lat_lon = {'lat': float(decimal_lat), 'lon': float(decimal_lon)}
    return lat_lon

def get_amenities():
    file_name = "amenities-vancouver.json.gz"
    amenities = pd.read_json(file_name, lines=True, compression='gzip')
    amenities = amenities[~amenities['name'].isnull()]
    amenities = amenities.reset_index(drop = True)
    return amenities

def get_amenities_with_wikidata():
    file_name = "amenities-vancouver.json.gz"
    amenities = pd.read_json(file_name, lines=True, compression='gzip')
    wikidata = pd.read_json('wikidata.json.gz', lines=True, compression='gzip')
    non_empty_tags = amenities[amenities['tags'] != {}]['tags'].rename('code')
    KEY = 'brand:wikidata'
    codes = non_empty_tags.apply(lambda d: d[KEY] if KEY in d else None)
    code_values = codes[~codes.isna()]
    amenities['wiki_code'] = codes
    wikidata = wikidata.rename(columns={'code': 'wiki_code'})
    result = pd.merge(amenities, wikidata, on='wiki_code', how='outer')
    result = result[~result['name'].isnull()]
    result = result.reset_index(drop = True)
    return result

def get_category_amenities(amenities, category):
    family_friendly = ['arts_centre', 'bbq', 'bistro', 'cafe', 'clock', 'community_centre', 'family_centre', 'ice_cream', 'juice_bar', 'leisure', 'library', 'marketplace', 'park', 'photo_booth', 'playground', 'restaurant', 'seaplane_terminal', 'theatre', 'science']
    foodie = ['bbq', 'bistro', 'cafe', 'restaurant']
    dessert_crawl = ['cafe', 'ice_cream']
    pub_crawl = ['bar', 'biergarten', 'pub']
    tourist = ['clock', 'cafe', 'seaplane_terminal', 'park', 'marketplace', 'leisure', 'restaurant', 'biergarten', 'theatre', 'science']

    if category == "family_friendly":
        return amenities[amenities['amenity'].isin(family_friendly)]
    elif category == "foodie":
        return amenities[amenities['amenity'].isin(foodie)]
    elif category == "dessert_crawl":
        return amenities[amenities['amenity'].isin(dessert_crawl)]
    elif category == "tourist":
        return amenities[amenities['amenity'].isin(tourist)]
    elif category == "pub_crawl":
        return amenities[amenities['amenity'].isin(pub_crawl)]
    else:
        return amenities

# output amenities within a square with length 2*N_meters surrounding the photo's coordinates
def get_nearby_amenities(photo_name, N_meters, amenities):
    gps_exif = get_gps_exif(photo_name)
    lat_lon = get_lat_lon(gps_exif)

    # "Around Vancouver, one degree of latitude or longitude is about 10^5 meters. That will be a close enough conversion as we're estimating error…"
    # per hint given in Exercise 3 for calc_distance.py
    meter_per_degree = 100000
    
    # filter out amenities outside the square with length 2*N_meters
    distance_from_center_in_degrees = N_meters/meter_per_degree
    amenities = amenities[amenities['lat'] <= lat_lon['lat']+distance_from_center_in_degrees]
    amenities = amenities[amenities['lat'] >= lat_lon['lat']-distance_from_center_in_degrees]
    amenities = amenities[amenities['lon'] <= lat_lon['lon']+distance_from_center_in_degrees]
    amenities = amenities[amenities['lon'] >= lat_lon['lon']-distance_from_center_in_degrees]
    
    return amenities

def get_n_random_locations(N, amenities):
    # return N random rows 
    return amenities.sample(N)

def get_coordinates_from_locations(amenities):
    coordinates_2d_list = amenities[['lat', 'lon']].values

    coordinates_tuples = []
    for item in coordinates_2d_list:
        coordinates_tuples.append(str(item[0]) + ',' + str(item[1]))

    return coordinates_tuples

def search_place(gps, api_key):
    # Send request 
    response = requests.get(
        'https://maps.googleapis.com/maps/api/place/nearbysearch/json?' + urlencode(
            {'location': gps, 'rankby': 'distance','key': api_key}))
 
    resp_address = response.json()
    
    if resp_address['status'] == 'OK':
        
        try:
            lat = resp_address['results'][0]['geometry']['location']['lat']
            lng = resp_address['results'][0]['geometry']['location']['lng']
            vicinity_addr = resp_address['results'][0]['vicinity']
            place_id = resp_address['results'][0]['place_id']
            place_rating = resp_address['results'][0]['rating']
            tot_reviews = resp_address['results'][0]['user_ratings_total']
            
        except KeyError:
            try:
                lat = resp_address['results'][1]['geometry']['location']['lat']
                lng = resp_address['results'][1]['geometry']['location']['lng']
                vicinity_addr = resp_address['results'][1]['vicinity']
                place_id = resp_address['results'][1]['place_id']
                place_rating = resp_address['results'][1]['rating']
                tot_reviews = resp_address['results'][1]['user_ratings_total']
                pass
            except KeyError:
                return[gps.split(",")[0], gps.split(",")[0], 'vicinity_addr', np.nan, np.nan, np.nan]
            except:
                return[gps.split(",")[0], gps.split(",")[0], 'vicinity_addr', np.nan, np.nan, np.nan]
        except:
            return[gps.split(",")[0], gps.split(",")[0], 'vicinity_addr', np.nan, np.nan, np.nan]
        
        return [lat, lng, vicinity_addr, place_id, place_rating, tot_reviews]
    else:
        print('Error occured:', resp_address)
        return ['Not found:', gps]

def search_place_nearby(gps, api_key):
    # Send request 
    response = requests.get(
    'https://maps.googleapis.com/maps/api/place/nearbysearch/json?' + urlencode(
        {'location': gps, 'rankby': 'distance', 'key': api_key}))
 
    resp_address = response.json()
    
    if resp_address['status'] == 'OK':    
            return resp_address
            
    else:
        print('Error occured:', resp_address)
        return ['Not found:', gps]

def getInfoFromGmapList(ls):
    newlist = []
    
    for i in range(len(ls)):
        for j in range(len(ls[i]['results'])):
            try:
                lat = ls[i]['results'][j]['geometry']['location']['lat']
                lng = ls[i]['results'][j]['geometry']['location']['lng']
                vicinity_addr = ls[i]['results'][j]['vicinity']
                name = ls[i]['results'][j]['name']
                category = ls[i]['results'][j]['types']
                place_id = ls[i]['results'][j]['place_id']
                place_rating = ls[i]['results'][j]['rating']
                tot_reviews = ls[i]['results'][j]['user_ratings_total']
                newlist.append([lat, lng, vicinity_addr, name, category, place_id, place_rating, tot_reviews])
            except KeyError:
                newlist.append([lat, lng, vicinity_addr, name, category, np.nan, np.nan, np.nan])
                pass
            
    return newlist

def find_place(gps, api_key):
    # Send request 
    response = requests.get('https://maps.googleapis.com/maps/api/geocode/json?' + urlencode(
        {'latlng': gps, 'key': api_key}))
 
    resp_address = response.json()
    
    # check if status is ok
    if resp_address['status'] == 'OK':
        return resp_address
        lat = resp_address['results'][0]['geometry']['location']['lat']
        lng = resp_address['results'][0]['geometry']['location']['lng']
        
        try:
            vicinity_addr = resp_address['results'][0]['vicinity']
            place_id = resp_address['results'][0]['place_id']
            place_rating = resp_address['results'][0]['rating']
            tot_reviews = resp_address['results'][0]['user_ratings_total']
            
        except KeyError:
            try:
                vicinity_addr = resp_address['results'][1]['vicinity']
                place_id = resp_address['results'][1]['place_id']
                place_rating = resp_address['results'][1]['rating']
                tot_reviews = resp_address['results'][1]['user_ratings_total']
                pass
            except KeyError:
                return[gps.split(",")[0], gps.split(",")[0], 'vicinity_addr', 'NA', 'NA', 'NA']
            except:
                return[gps.split(",")[0], gps.split(",")[0], 'vicinity_addr', 'NA', 'NA', 'NA']
        except:
            return[gps.split(",")[0], gps.split(",")[0], 'vicinity_addr', 'NA', 'NA', 'NA']
        
        return [lat, lng, vicinity_addr, place_id, place_rating, tot_reviews]
    else:
        print('Error occured:', resp_address)
        return ['Not found:', gps]

def get_place_details(place_id, api_key):
    # Send request by API
    response = requests.get(
        'https://maps.googleapis.com/maps/api/place/details/json?' + urlencode(
            {'place_id': place_id, 'key': api_key}))
    # Read response as json
    resp_details = response.json()
    return resp_details
    # status=OK: the place was successfully detected and at least one result was returned
    for i in range(len(resp_details)):

        if resp_details['status'] == 'OK':
            for i in range(len(resp_details)):
                review_rating = resp_details['result']['reviews'][i]['rating']
                review_time = resp_details['result']['reviews'][i]['relative_time_description']
                review_timestamp = resp_details['result']['reviews'][i]['time']
                review_text = resp_details['result']['reviews'][i]['text']
                return [place_id, review_rating, review_time, review_timestamp, review_text]
        else:
            print('Failed to get json response:', resp_details)
            return ['Review is not found', place_id]

def parseNumbertoFloat(str):
    #from https://stackoverflow.com/questions/42142309/python-regex-to-get-float-number-from-string
    digit = re.findall(r"(?<![a-zA-Z:])[-+]?\d*\.?\d+", str)
    
    if("km" in str):
        return float(digit[0])
    
    else:
        return float(digit[0])/1000

def AddHourTime(str):
    hour = str.split("hour")[0]
    
    #Check if there really has a split, else it means there are 0 hours
    if (hour == str):
        hour = 0
    
    return int(hour)

def AddMinTime(str):
    time = str.split("hour")
    
    #Check if there really has a split, else it means there are 0 hours and only minutes
    if (time[0] == str):
        #Remove the 'mins' word
        min = time[0].split("min")[0]
    else:
        #Remove the 'mins' word
        min = parseNumbertoFloat(time[1].split("min")[0])
    
    return int(min)

def removeBracket(str):
        return re.sub(r'\<.*?>', '', str)

def haveBoringCategories(ls):
    boring_categories_tourist = ['station', 'category', 'insurance', 'bank', 'contractor', 
                                 'moving_company', 'repair', 'finance', 'dentist', 'learning', 'storage',
                                 'health', 'church', 'lawyer', 'real_estate_agency', 'shoe_store']

    #remove any categories that at least have the word in the boring_categoires_tourist
    #For example, bus_station will also be removed
    for i in ls:
        if any(word in i.lower() for word in boring_categories_tourist):
             return True
        else:
             continue
    return False

def haveBoringNames(ls):
    boring_categories_tourist = ['station', 'insurance', 'bank', 'contractor', 'wedding',
                                'repair', 'financial', 'dentist', 'learning', 'daycare', 'property', 
                                'child', 'tattoo', 'ovation', 'reading', 'realtor', 'photography',
                                 'grocery', 'inc.', 'apartments']

    #remove any categories that at least have the word in the boring_categoires_tourist
    #For example, bus_station will also be removed
    if any(word in ls.lower() for word in boring_categories_tourist):
        return True
    
    return False

def get_travel_df(response):
    start_address = []
    end_address = []

    distance_to_travel = []
    travel_time = []

    for i in range(len(response['legs'])):
        start_address.append(response['legs'][i]['start_address'])
        end_address.append(response['legs'][i]['end_address'])
        distance_to_travel.append(response['legs'][i]['distance']['text'])
        travel_time.append(response['legs'][i]['duration']['text'])

    # Store the parsed infromation into a dataframe
    travel_df = pd.DataFrame({
        'start_address' : start_address, 
        'end_address' : end_address,
        'distance' : distance_to_travel,
        'duration' : travel_time
    })

    # Add the distance in kilometers for the travel
    travel_df['distance_km'] = travel_df['distance'].apply(parseNumbertoFloat)

    # Add the hour and minutes of the travel duration
    travel_df['hour'] = travel_df['duration'].apply(AddHourTime)
    travel_df['minutes'] = travel_df['duration'].apply(AddMinTime)

    return travel_df

def print_travel_info(travel_df):
    # Compute total distance and total hours and minutes of the travel
    total_distance = sum((travel_df['distance_km']))
    total_distance = round(total_distance, 2)

    total_minutes = sum(travel_df['minutes'])
    total_hours = sum(travel_df['hour']) 

    # Convert it into total n hours and x mins form
    total_hours = total_hours + total_minutes//60
    total_minutes = total_minutes%60

    print("Total Distance:", total_distance, "km")
    print("Estimated time needed (by cycling):" ,total_hours, "Hours", total_minutes, "Minutes")

def get_route_df(response):
    # Parse the step-by-step route information
    start_lat =[]
    start_lon = []

    end_lat = []
    end_lon = []

    distance = []
    duration = []
    instructions = []

    for i in range(len(response['legs'])):
        for j in range(len(response['legs'][i]['steps'])):
            start_lat.append(response['legs'][i]['steps'][j]['start_location']['lat'])
            start_lon.append(response['legs'][i]['steps'][j]['start_location']['lng'])
        
            end_lat.append(response['legs'][i]['steps'][j]['end_location']['lat'])
            end_lon.append(response['legs'][i]['steps'][j]['end_location']['lng'])
        
            distance.append(response['legs'][i]['steps'][j]['distance']['text'])
            duration.append(response['legs'][i]['steps'][j]['duration']['text'])
            instructions.append(response['legs'][i]['steps'][j]['html_instructions'])

    # Store route information in a dataframe
    route_df = pd.DataFrame({
        'start_lat' : start_lat,
        'start_lon' : start_lon,
        'end_lat' : end_lat,
        'end_lon' : end_lon,
        'distance' : distance,
        'duration' : duration,
        'instructions' : instructions
    })

    # Clean up the instructions
    route_df['instructions'] = route_df['instructions'].apply(removeBracket)

    return route_df

def get_route_with_waypoints(origin, waypoints, end=None):
    original_location = str(origin['lat']) + ',' + str(origin['lon'])

    end_location = None
    if end:
        end_location = str(end['lat']) + ',' + str(end['lon']) 

    # Perform the request
    now = datetime.now()
    directions_result = GMAPS_DIR.directions(
        origin=original_location,
        destination=(original_location if end_location == None else end_location),
        waypoints=waypoints,
        mode= "bicycling",
        departure_time=now
    )

    # Return the response
    return directions_result[0]

def get_route(origin, end):
    original_location = str(origin['lat']) + ',' + str(origin['lon'])
    # Get the friend's location
    end_location = str(end['lat']) + ',' + str(end['lon']) 

    # Get current time for request
    now1 = datetime.now()

    # Perform the request
    directions_result = GMAPS_DIR.directions(
        origin= original_location,
        destination= end_location,
        mode= "bicycling",
        departure_time=now1
    )

    # Return the response
    return directions_result[0]

def print_recommended(df, p1, p2, k1, k2):
    print(f"{p1} {df.iloc[0][k1]} at {df.iloc[0][k2]}\n")
    print(p2)
    for i in range(1,4):
        print(f"{df.iloc[i][k1]} at {df.iloc[i][k2]}")
