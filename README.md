# Bike Tour YVR

## Description
- Your friend gives you a photo of some location in the Vancouver area
- Using a photo you've taken of your current location, Bike Tour YVR will produce a route to your friends location, along with some interesting amenities to check out along the way

## Usage
- Our project's features and findings can be found in the `BikeTourYVR.ipynb` notebook.
- Simply restart and run-all cells to run the notebook with the default photos, or add your own photos to this directory, and set the values to your photos at the beginning of the notebook.

## Output
- A map is produced in this notebook, displaying a return route from the users location to their friend's, along with the recommened amenities to visit along the way.

## Requirements
- Before running this notebook, please run `$ pip3 install -r requirements.txt` to install the required libraries

### Notebook Extensions
- This notebook requires ipyleaflet (and ipywidgets)
- Full installation instructions can be found here: https://ipyleaflet.readthedocs.io/en/latest/installation.html

### Steps taken to run BikeTourYVR.ipynb
1. Install ipyleaflet (this should also install ipywidgets):
- Run `$ pip3 install -r requirements.txt` to install ipyleaflet along with all other requirements

2. Enable notebook extensions:
- `$ jupyter nbextension install --py --symlink --sys-prefix ipyleaflet`
- `$ jupyter nbextension enable --py --sys-prefix ipyleaflet`

3. You may also need to enable the following, if the steps above didn't work:
- `$ jupyter nbextension install --py widgetnbextension --sys-prefix widgetnbextension`
- `$ jupyter nbextension enable --py --sys-prefix widgetnbextension`

### NOTE
- Depending on your setup, the commands above may require `sudo` privileges
- Steps 2 and 3 may not be necessary depending on your version of Jupyter-Notebook