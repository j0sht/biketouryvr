from ipywidgets import HTML
from ipyleaflet import Map, Marker, MarkerCluster, AntPath

SW = 'southwest'
NE = 'northeast'

LAT = 'lat'
LON = 'lng'

START = 'start_location'
END = 'end_location'

def get_locations(legs):
    result = []
    for leg in legs:
        leg_start = leg[START]
        leg_end = leg[END]
        result.append([leg_start[LAT], leg_start[LON]])
        
        steps = leg['steps']
        for step in steps:
            step_start = step[START]
            step_end = step[END]
            result.append([step_start[LAT], step_start[LON]])
            result.append([step_end[LAT], step_end[LON]])
        
        result.append([leg_end[LAT], leg_end[LON]])
    
    return result

def get_amenity_from_string(s):
    """
    Returns the amenity space seperated, with each word capitalized

    Expects the amenity to be seperated by underscores
    
    >>> get_amenity('point_of_interest')
    Point Of Interest
    """
    amenity = s
    split = amenity.split('_')
    return ' '.join(split).title()

def get_amenity(ls):
    """
    Returns the first amenity in given category list
    
    Expects the amenity to be seperated by underscores
    
    Returns the amenity space seperated, with each word capitalized
    
    >>> get_amenity(['point_of_interest', 'establishment'])
    Point Of Interest
    """
    amenity = ls[0]
    split = amenity.split('_')
    return ' '.join(split).title()


def get_recommended_places_basic(df):
    """
    Converts recommended places to visit to a list of dictionary records,
    with keys: 'location', 'amenity', 'name'
    """
    result = df[['lat', 'lon', 'name', 'amenity', 'ratings', 'num_reviews']].to_dict('records')

    for record in result:
        record['location'] = (record['lat'], record['lon'])
        del record['lat']
        del record['lon']
        # Only keep the first category to display in map
        record['amenity'] = get_amenity_from_string(record['amenity'])

    return result

def get_recommended_places(df):
    """
    Converts recommended places to visit to a list of dictionary records,
    with keys: 'location', 'amenity', 'name'
    """
    result = df[['lat', 'lon', 'Name', 'Category', 'ratings', 'num_reviews']].to_dict('records')

    for record in result:
        record['location'] = (record['lat'], record['lon'])
        del record['lat']
        del record['lon']
        # Only keep the first category to display in map
        record['amenity'] = get_amenity(record['Category'])
        del record['Category']
        record['name'] = record['Name']
        del record['Name']

    return result

def get_center(bounds):
    lat = (bounds[SW][LAT] + bounds[NE][LAT]) / 2
    lon = (bounds[SW][LON] + bounds[NE][LON]) / 2
    return (lat, lon)

def get_marker_cluster(poi):
    markers = []
    
    for p in poi:
        # Create marker
        marker = Marker(location=p['location'])

        # Create message for marker
        message = HTML()
        amenity = p['amenity'].capitalize()
        name = p['name']
        rating = p['ratings']
        num_reviews = int(p['num_reviews'])
        s = f"<b>{amenity}</b><br>{name}<br><b>Rating</b><br>{rating} / 5.0 ★<br><b>Number of Reviews</b><br>{num_reviews}"
        message.value = s

        # Add message to marker
        marker.popup = message

        # Add marker to list
        markers.append(marker)

    return MarkerCluster(markers=markers)

def display_map(poi, response):
    # Center the map on the route in the respons
    center = get_center(response['bounds'])
    m = Map(center=center, zoom=12)

    # Get route information and create route layer
    legs = response['legs']
    leg_locations = get_locations(legs)
    route = AntPath(
        locations=leg_locations,
        dash_array=[1, 10],
        delay=1000,
        color='#7590ba',
        pulse_color='#3f6fba'
    )
    m.add_layer(route)

    # Add markers for points of interest
    marker_cluster = get_marker_cluster(poi)
    m.add_layer(marker_cluster)

    # Display the map
    display(m)
