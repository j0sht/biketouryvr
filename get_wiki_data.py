"""
Script to find wikidata for code values in the amenities-vancouver data

Assumes that the file 'codes.csv.gz' is in your home HDFS DFS directory

The following code was adapted from Greg Baker's "transform_download.py"
"""
import sys
import json

assert sys.version_info >= (3, 5) # make sure we have Python 3.5+
from pyspark.sql import SparkSession, functions, types
spark = SparkSession.builder.appName('wikidata').getOrCreate()
assert spark.version >= '2.3' # make sure we have Spark 2.3+
spark.sparkContext.setLogLevel('WARN')
sc = spark.sparkContext

def get_data(e):
    data = {'code': e['id']}

    try:
        data['description'] = e['descriptions']['en']['value']
    except KeyError:
        pass

    try:
        data['labels'] = e['labels']['en']['value']
    except KeyError:
        pass

    try:
         val_list = e['aliases']['en']
         values = list(map(lambda d: d['value'] if d['value'] else '', val_list))
         data['aliases'] = values
    except KeyError:
        pass

    return data

def main(inputs, output):
    # Read wikidata lines
    lines = sc.textFile(inputs)
    lines = lines.filter(lambda l: len(l) > 1) # throw away initial '[' and ']' lines
    lines = lines.map(lambda l: l[:-1] if l[-1] == ',' else l)

    # Get entities
    entities = lines.map(json.loads)
    
    # Read codes
    codes = spark.read.csv('codes.csv.gz', header=True)

    # There are only ~100 code values, so it should be safe to read them in as a set
    codes_set = set(map(lambda c: c['code'], codes.collect()))

    # Find matching codes in entities
    matching = entities.filter(lambda e: e['id'] in codes_set)

    # Extract relevant data
    data = matching.map(get_data)
    data = data.map(json.dumps)

    # Save to file
    data.saveAsTextFile(output, compressionCodecClass='org.apache.hadoop.io.compress.GzipCodec')

if __name__ == '__main__':
    inputs = sys.argv[1]
    output = sys.argv[2]
    main(inputs, output)
