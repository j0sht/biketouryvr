"""
This is a little script to get the codes from the tags column
in 'amenities-vancouver.json.gz'

It assumes that the file is in the same directory.

Writes the codes to a file called 'codes.csv.gz'
Writes dataframe with codes column to file
"""
import pandas as pd

# Key for the wikidata code value in tag dictionary
KEY = 'brand:wikidata'

# Read amenities-vancouver
df = pd.read_json('amenities-vancouver.json.gz', lines=True, compression='gzip')

# Get non-empty tag dictionaries
non_empty_tags = df[df['tags'] != {}]['tags'].rename('code')

# Get code values from each row
codes = non_empty_tags.apply(lambda d: d[KEY] if KEY in d else None)
code_values = codes[~codes.isna()]

# Add code column to dataframe
df['code'] = code_values

# Filter rows without a code
amenities_with_code = df[~df['code'].isna()]

# Filter rows where name is NaN
# (Rows where name is NaN are charging stations, and an atm)
amenities_with_code = amenities_with_code[~amenities_with_code['name'].isna()]

# Write data to file
code_values.to_csv('codes.csv.gz', index=False, compression='gzip')
amenities_with_code.to_json('amenities_with_code.json.gz', orient='records', compression='gzip')
